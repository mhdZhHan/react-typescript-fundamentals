import "./App.css"
import Button from "./components/Button"
import Container from "./components/Container"
import Greet from "./components/Greet"
import Heading from "./components/Heading"
import Input from "./components/Input"
import Person from "./components/Person"
import PersonList from "./components/PersonList"
import Status from "./components/Status"
import Wrapper from "./components/Wrapper"
import Counter from "./components/state/Counter"

import Box from "./components/context/Box"
import { TheContextProvider } from "./components/context/ThemeContext"

import User from "./components/context/User"
import { UserContextProvider } from "./components/context/UserContext"

import ClassCounter from "./components/class/Counter"
import Private from "./components/auth/Private"
import Profile from "./components/auth/Profile"
import List from "./components/generics/List"
import RandomNumbers from "./components/restrictions/RandomNumbers"

import NewButton from "./components/html/Button"

function App() {
	const personName = {
		first: "Mohammed",
		last: "Shajahan",
	}

	const namesList = [
		{
			first: "Mohammed",
			last: "Shajahan",
		},
		{
			first: "Liyana",
			last: "Fathima",
		},
		{
			first: "Zahra",
			last: "Fathima",
		},
	]

	return (
		<div className="App">
			<Greet name="Ali" isLoggedIn={true} />
			<Person name={personName} />
			<PersonList names={namesList} />
			<Status status="loading" />
			<Heading>Heading component with children text</Heading>
			<Wrapper>
				<Heading>Heading inside wrapper</Heading>
			</Wrapper>
			<Button
				handleClick={(event, id) =>
					console.log("Button clicked", event, id)
				}
			/>
			<Input
				value=""
				handleChange={(event) => console.log(event.target.value)}
			/>
			<Container styles={{ border: "1px solid #000", padding: "1rem" }} />
			<h2>useReducer</h2>
			<Counter />

			<TheContextProvider>
				<Box />
			</TheContextProvider>

			<UserContextProvider>
				<User />
			</UserContextProvider>

			<hr />

			<ClassCounter message="Hello class component!" />

			<hr />

			<Private isLoggedIn={true} component={Profile} />

			<hr />

			<List
				items={["apple", "orange", "greps"]}
				onClick={(item) => console.log(item)}
			/>
			<List items={[1, 2, 3, 4]} onClick={(item) => console.log(item)} />

			<hr />

			<RandomNumbers value={10} isPositive />

			<hr />

			<NewButton variant="secondary" onClick={() => console.log("Hello")}>
				Hello Secondary button
			</NewButton>
		</div>
	)
}

export default App
