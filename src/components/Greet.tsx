type GreetProps = {
	name: string
	messageCount?: number
	isLoggedIn: boolean
}

export default function Greet(props: GreetProps) {
	const { messageCount = 0 } = props
	return (
		<div>
			<h2>
				{props.isLoggedIn
					? `Welcome ${props.name}! you have ${messageCount} unread messages`
					: "welcome Guest"}
			</h2>
		</div>
	)
}
