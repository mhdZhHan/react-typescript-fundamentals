import { PersonProps } from "./Person.types"

export default function Person({ name: { first, last } }: PersonProps) {
	return (
		<div>
			{first} {last}
		</div>
	)
}
