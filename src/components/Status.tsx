type StatusProps = {
	status: "loading" | "success" | "error"
}

function Status(props: StatusProps) {
	let messages: string = ""

	if (props.status === "loading") {
		messages = "Loading..."
	} else if (props.status === "success") {
		messages = "Data fetched successfully"
	} else if (props.status === "error") {
		messages = "Error fetching data"
	}

	return <div>Status - {messages}</div>
}

export default Status
