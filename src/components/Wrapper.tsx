type WrapperProps = {
	children: React.ReactNode
}

function Wrapper({ children }: WrapperProps) {
	return <div>{children}</div>
}

export default Wrapper
