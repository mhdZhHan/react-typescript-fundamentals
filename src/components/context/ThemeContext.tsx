import { createContext } from "react"
import { theme } from "./theme"

type TheContextProviderProps = {
	children: React.ReactNode
}

export const ThemeContext = createContext(theme)

export const TheContextProvider = ({ children }: TheContextProviderProps) => {
	return (
		<ThemeContext.Provider value={theme}>{children}</ThemeContext.Provider>
	)
}
