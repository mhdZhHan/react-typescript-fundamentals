import Greet from "../Greet"

const ComponentProps = (props: React.ComponentProps<typeof Greet>) => {
	return <div>{props.name}</div>
}

export default ComponentProps
