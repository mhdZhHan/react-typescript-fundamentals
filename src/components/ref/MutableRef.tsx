import { useEffect, useRef, useState } from "react"

const MutableRef = () => {
	const [timer, setTimer] = useState(0)

	const intervalRef = useRef<number | null>(null)

	const stopTimer = () => {
		if (intervalRef.current) window.clearInterval(intervalRef.current)
	}

	useEffect(() => {
		intervalRef.current = window.setInterval(() => {
			setTimer((prev) => prev + 1)
		}, 100)

		return () => {
			stopTimer()
		}
	})

	return (
		<div>
			Timer - {timer} -<button onClick={stopTimer}>Stop</button>
		</div>
	)
}

export default MutableRef
